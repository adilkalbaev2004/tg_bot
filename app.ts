import { Pool } from "pg";
import express from "express";
import bodyParser from "body-parser";
import TelegramBot from "node-telegram-bot-api";
import dotenv from "dotenv";
import cors from "cors";

dotenv.config();

const app = express();
app.use(cors());
app.use(bodyParser.json());
//
const port = process.env.PORT || 3000;
const telegramBotToken = process.env.TGBOT_TOKEN || "";
const bot = new TelegramBot(telegramBotToken, { polling: true });
const pool = new Pool({
  user: process.env.PGUSER,
  host: process.env.PGHOST,
  database: process.env.PGDATABASE,
  password: process.env.PGPASSWORD,
  port: 5432,
});

const apiRouter = express.Router();
app.use("/api", apiRouter);

bot.onText(/\/start/, async (msg) => {
  const chatId = msg.chat.id;

  const questions = await pool.query('SELECT * FROM "questions"');

  const keyboard = questions.rows.map((question) => [
    {
      text: question.questionText,
      callback_data: question.questionID.toString(),
    },
  ]);

  bot.sendMessage(chatId, "Выберите вопрос:", {
    reply_markup: {
      inline_keyboard: keyboard,
    },
  });
});

bot.on("callback_query", async (query) => {
  const chatId = query.message?.chat.id;
  const questionID = query.data;

  if (chatId !== undefined) {
    const answers = await pool.query(
      'SELECT * FROM "answers" WHERE "questionID" = $1',
      [questionID]
    );

    let response = "Нет ответа на этот вопрос.";
    if (answers.rows.length > 0) {
      response = answers.rows[0].answerText;
    }

    bot.sendMessage(chatId, response);
  } else {
    console.error("Ошибка: chatId не определен");
  }
});

app.listen(port, () => {
  console.log(`Сервер запущен на порту ${port}`);
});
